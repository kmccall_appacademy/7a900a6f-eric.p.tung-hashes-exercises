# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  result_hash = Hash.new
  str.split(" ").each { |word| result_hash[word] = word.length }
  result_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  new_keys = newer.keys
  older.each do |k_old, v_old|
    if newer.keys.include?(k_old)
      older[k_old] = newer[k_old]
      new_keys.delete(k_old)
    end
  end
  new_keys.each { |k_new| older[k_new] = newer[k_new] }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  counter=Hash.new(0)
  word.chars.each { |letter| counter[letter]+=1 }
  counter
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  counter_hash = Hash.new(0)
  arr.each { |el| counter_hash[el]+=1 }
  counter_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_or_odd = { even: 0 , odd: 0 }
  numbers.each { |num| (num%2 == 0 ? even_or_odd[:even]+=1 : even_or_odd[:odd]+=1) }
  even_or_odd
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  counter_hash = Hash.new(0)
  string.chars.each { |letter| counter_hash[letter]+=1 }
  counter_hash.sort_by { |k, v| k }.reverse
  counter_hash.sort_by { |k, v| v }.last.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  students.select { |k, v| v > 6 }.keys.to_a.combination(2)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  counter_hash = Hash.new(0)
  specimens.each { |animal| counter_hash[animal]+=1 }
  smallest_population_size = counter_hash.sort_by { |k, v| v}.first.last
  largest_population_size = counter_hash.sort_by { |k, v| v}.last.last
  counter_hash.to_a.length**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_counter=Hash.new(0)
  vandalized_counter=Hash.new(0)
  normal_sign.downcase.chars.each { |letter| normal_counter[letter]+=1 }
  vandalized_sign.downcase.chars.each { |letter| vandalized_counter[letter]+=1 }
  vandalized_counter.each { |k, v| return false if normal_counter[k].nil? || v > normal_counter[k] }
  true
end

def character_count(str)
end
